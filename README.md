# DeepLearner [![wakatime](https://wakatime.com/badge/github/adamwillisXanax/DeepLearner.svg)](https://wakatime.com/badge/github/adamwillisXanax/DeepLearner)
A Deep Learning Repository, you can also view the [project](https://github.com/users/adamwillisXanax/projects/1/views/1)


## 🗺 legend
(): optional
<br />
[]: important
<br />
 

Life is a Set of random variables of `opportunities` 
[FYI: random Variable is 
1.Neither a Random: always follow a distribution
2. Nor a Variable: it is a function of Probability Density(_**me**:frequency_) 
]
the objective is to Utilze the most of it, in an _effective manner_- __~90%__ of the time (_Almost Surely_) 

### 🎮 Mixed-Coding mode
**1.Focus**
The best learning is that One where pushing self, to face off th  Poly-shaped, Spaghetti monster- (like this one) 

**2.Relax**
Space in mind, in call for relaxation, with Early _signs_ of: 
- Once `Creativity` starts lacking, - when new ideas become `transparent` 
- `Novelity` vanishes into thin air, & Cloud of Routine takes over, its shadows become in everywhere you go  that's when it Hovers over 

## what I'm learning with this project

- [x]  Use an [acceptable] RNG (Random Number Generator) ( `StableRNG`)

- [ ] How to build A NeuralNetwork Model?

- [ ] Which differentiation Module to Pick?
*Thinking about it* 🤔


# Credits

## Special Thanks
to the Heros behind the Scene; a list of Human Beings, in which I couldn't do this project without, 
in the Alphabetic Order:

- Professor `Steven G. Johnson` [@stevengj](https://github.com/stevengj): providing an MIT OCW `Introduction to Numerical Methods` [18.335J / 6.337J](https://ocw.mit.edu/courses/mathematics/18-335j-introduction-to-numerical-methods-spring-2019/)  Julia course (usefully Concise Course) on [github](https://github.com/mitmath/18335/tree/spring19)
- `Tamas K. Papp [PhD]` [@tpapp](https://github.com/tpapp): Useful Code Help  on the `Julia Discourse` 
- `Professor Tim Holy` (Neurosciene) [@timholy](https://github.com/timholy): help with Arrays (& Holmes) (as featured, in  [juliacon2016 Keynote - an overview of Arrays](https://www.youtube.com/watch?v=fl0g9tHeghA))


## Lessons learned 

- `Ziggurat function` is a **Biased** Data Sampler. Samples are More around the Mean, whereas an Ideal  Sampler would fairly 
but there aren't as much are  the `Tail Area`. An Ideal sampler would `Uniformly Samples` (on x-Axis), regardless the Distribution & its respective properties  (for that reason, I don't see it as an enhancement). _Alas_, to Cross-reference with another paper (yet to come) that studies Tails, & proves this algorithm is sound, & Fairly Samples data in tail, just as everywhere else

- Opens up a _possibility_ for a **Statistical Repo**
- For more info, please visit [the Discussion](https://github.com/adamwillisXanax/DeepLearner/discussions/12)

## Papers Used 

-[The Ziggurat paper](https://arxiv.org/abs/1403.6870), by Christopher D McFarland : `A modified ziggurat algorithm for generating exponentially and
Normally Distributed Pseudorandom Numbers` (including the author's view on the [Tail generation issue](https://github.com/adamwillisXanax/DeepLearner/discussions/12))

Good points of this paper is: it does not use rejection regions (that what makes it fancy to apply it)Please Review (if you will) the paper,on p.3:
>We do not improve upon these approaches here and, instead, _reuse previous techniques_
>...
>Overall, the ZA is ideal for distributions
>_with Infrequent Sampling from the tail_, i.e. **not heavy-tailed distributions.**

**Paper on could be found _[from here](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4812161/pdf/nihms-717849.pdf)_**


A modified ziggurat algorithm for generating exponentially- and  normally-distributed Pseudorandom Numbers
## Youtube
Personally, the best Neural Network course, easily Explained by a Humble `Prof. S. Sengupta`, (what S. stands for?), by the NPTEL of India [right here](https://www.youtube.com/watch?v=xbYgKoG4x2g&list=PL53BE265CE4A6C056)
# A WIP (Work In Progress) Project
This project is just a Sown`seed`
<br/>
as one remembers, Or on a `Social Network`
If you'd like to  give a `hand`,
Please **step in**; Your Help would be much `Appreciated`-  **_Thank You_**
_(though getting into the state of `Figuring things Out` has a mesmerizing remains, in its calling )_

## Disclaimer
The author won't not be held responsible, for any immature actions, & or signs of code abuse, at all costs

## Author

[![Logo](https://github.com/adamwillisXanax/adamwillisXanax/blob/main/Assets/logo.png)
](https://github.com/adamwillisXanax/adamwillisXanax)
